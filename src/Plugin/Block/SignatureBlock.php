<?php

namespace Drupal\tiz_signature\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SignatureBlock' block.
 *
 * @Block(
 *  id = "signature_block",
 *  admin_label = @Translation("Signature block"),
 * )
 */
class SignatureBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'signature_block';

    return $build;
  }

}
