# README #

### What is this repository for? ###

Add a block that display the latest Tiz signature. 

### How do I get set up? ###

- `composer config repositories.tiz composer http://packages.tiz.fr` (to enable tiz packages)
- `composer config secure-http false` (as http://packages.tiz.fr does not have https yet)
- `composer require tiz_dr8/tiz_signature`
- enable module
- attach block to target region

### TODOs ###

- set svg fill color and height values as config